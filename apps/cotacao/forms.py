#-*- coding: utf-8 -*-
from django import forms
from django.utils.translation import ugettext as _
from django.utils.translation import ugettext_lazy as _

EMAIL_CHOICES = (
        ('',_('Selecione')),
        ('textile@equalizeltd.com', 'textile@equalizeltd.com'),
        ('construction@equalizeltd.com', 'construction@equalizeltd.com'),
        ('electronics@equalizeltd.com', 'electronics@equalizeltd.com'),
        ('gifts@equalizeltd.com', 'gifts@equalizeltd.com'),
        ('info@equalizeltd.com', 'info@equalizeltd.com'),
    )

class CotacaoForm(forms.Form):

    empresa = forms.CharField(label=_('Nome da empresa'), max_length=150, widget=forms.TextInput(attrs={'placeholder': _('Nome da empresa'),'class':'col-xs-12 required'}))
    contato = forms.CharField(label=_('Contato'), widget=forms.TextInput(attrs={'placeholder': _('Contato'),'class': 'col-xs-12 required'}))
    email = forms.EmailField(label=_('Seu email'), max_length=200, widget=forms.TextInput(attrs={'placeholder': _('Seu E-mail'),'class':'col-xs-12 required email esq'}))
    departamento = forms.ChoiceField(label=_('Departamento'), choices=EMAIL_CHOICES, widget=forms.Select(attrs={'class':'col-xs-12 required email esq'}))
    mensagem = forms.CharField(widget=forms.Textarea(attrs={'placeholder': _('Mensagem'),'class':'col-xs-12 required', 'cols':'', 'rows':'10'}),label=_('Mensagem'))
    arquivo = forms.FileField(label=_('Arquivo'), widget=forms.FileInput(attrs={'placeholder' : _('Selecione um arquivo')}))
    fake_file = forms.CharField(widget=forms.TextInput(attrs={'placeholder': _('Selecione um arquivo'), 'class':'col-xs-12'}))

    
