#-*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
from apps.cotacao.views import *
from django.utils.translation import ugettext_lazy as _


urlpatterns = patterns('apps.cotacao.views',
    url(_(r'^cotacao/$'), CotacaoView.as_view(), name='cotacao'),
    url(r'^m/([0-9]{1})$', CotacaoView.as_view(), name='enviado'),

)