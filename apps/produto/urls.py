#-*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
from apps.produto.views import *

from django.utils.translation import ugettext_lazy as _

urlpatterns = patterns('apps.produto.views',

    url(_(r'^textil/$'), TextilListView.as_view(), name="textil"),
    url(_(r'^eletronicos/$'), EletronicoListView.as_view(), name="eletronico"),
    url(_(r'^construcao/$'), ConstrucaoListView.as_view(), name="construcao"),
    url(_(r'^promocionais/$'), PromocionalListView.as_view(), name="promocional"),
    url(_(r'^bens-de-consumo/$'), ConsumerGoodsListView.as_view(), name="consumer_goods"),
    url(_(r'^alimentos-e-bebidas/$'), FoodBeverageListView.as_view(), name="foods_beverages"),

)
