# -*- coding:utf-8 -*-
from apps import settings
from django.db import models
import datetime
import os
from uuid import uuid4
from django.utils.deconstruct import deconstructible
from ckeditor.fields import RichTextField
from django.contrib.auth.models import User
from sorl.thumbnail import ImageField
from django.template.defaultfilters import slugify

class Textil(models.Model):

    class Meta:
        verbose_name = 'Textil'
        verbose_name_plural = 'Texteis'

    titulo = models.CharField(u'Titulo', max_length=150)
    texto = RichTextField(u'Texto',blank=True,null=True)
    slug = models.SlugField(u'URL', max_length=150,blank=True,null=True,unique=True)
    idioma = models.CharField(u'Idioma', choices=settings.LANGUAGES, max_length=15)
    date_joined = models.DateTimeField('Criado em',auto_now_add=True)
    date_update = models.DateTimeField('Atualizado em',auto_now=True)

    def __unicode__(self):
        return self.titulo

class Eletronico(models.Model):

    class Meta:
        verbose_name = 'Eletronico'
        verbose_name_plural = 'Eletronicos'

    titulo = models.CharField(u'Titulo', max_length=150)
    texto = RichTextField(u'Texto',blank=True,null=True)
    slug = models.SlugField(u'URL', max_length=150,blank=True,null=True,unique=True)
    idioma = models.CharField(u'Idioma', choices=settings.LANGUAGES, max_length=15)
    date_joined = models.DateTimeField('Criado em',auto_now_add=True)
    date_update = models.DateTimeField('Atualizado em',auto_now=True)

    def __unicode__(self):
        return self.titulo

class Construcao(models.Model):

    class Meta:
        verbose_name = 'Construcao'
        verbose_name_plural = 'Construcoes'

    titulo = models.CharField(u'Titulo', max_length=150)
    texto = RichTextField(u'Texto',blank=True,null=True)
    slug = models.SlugField(u'URL', max_length=150,blank=True,null=True,unique=True)
    idioma = models.CharField(u'Idioma', choices=settings.LANGUAGES, max_length=15)
    date_joined = models.DateTimeField('Criado em',auto_now_add=True)
    date_update = models.DateTimeField('Atualizado em',auto_now=True)

    def __unicode__(self):
        return self.titulo

class Promocional(models.Model):

    class Meta:
        verbose_name = 'Promocional'
        verbose_name_plural = 'Promocionais'

    titulo = models.CharField(u'Titulo', max_length=150)
    texto = RichTextField(u'Texto',blank=True,null=True)
    slug = models.SlugField(u'URL', max_length=150,blank=True,null=True,unique=True)
    idioma = models.CharField(u'Idioma', choices=settings.LANGUAGES, max_length=15)
    date_joined = models.DateTimeField('Criado em',auto_now_add=True)
    date_update = models.DateTimeField('Atualizado em',auto_now=True)

    def __unicode__(self):
        return self.titulo

class ConsumerGoods(models.Model):

    class Meta:
        verbose_name = u'Bens e consumo'
        verbose_name_plural = u'Bens e consumo'

    titulo = models.CharField(u'Titulo', max_length=150)
    texto = RichTextField(u'Texto', blank=True, null=True)
    slug = models.SlugField(u'URL', max_length=150, blank=True, null=True, unique=True)
    idioma = models.CharField(u'Idioma', choices=settings.LANGUAGES, max_length=15)
    date_joined = models.DateTimeField('Criado em', auto_now_add=True)
    date_update = models.DateTimeField('Atualizado em', auto_now=True)

    def __unicode__(self):
        return self.titulo

class FoodBeverage(models.Model):

    class Meta:
        verbose_name = u'Alimento e bebida'
        verbose_name_plural = u'Alimentos e bebidas'

    titulo = models.CharField(u'Titulo', max_length=150)
    texto = RichTextField(u'Texto', blank=True, null=True)
    slug = models.SlugField(u'URL', max_length=150,blank=True,null=True,unique=True)
    idioma = models.CharField(u'Idioma', choices=settings.LANGUAGES, max_length=15)
    date_joined = models.DateTimeField('Criado em',auto_now_add=True)
    date_update = models.DateTimeField('Atualizado em',auto_now=True)

    def __unicode__(self):
        return self.titulo
