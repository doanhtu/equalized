# -*- coding:utf-8 -*-
from django.contrib import admin
from apps.produto.models import *
from apps.produto.forms import *

@admin.register(Textil)
class TextilAdmin(admin.ModelAdmin):
    list_display = ('titulo','idioma','date_update')
    prepopulated_fields = {"slug": ("titulo",)}

@admin.register(Eletronico)
class EletronicoAdmin(admin.ModelAdmin):
    list_display = ('titulo','idioma','date_update')
    prepopulated_fields = {"slug": ("titulo",)}

@admin.register(Construcao)
class ConstrucaoAdmin(admin.ModelAdmin):
    list_display = ('titulo','idioma','date_update')
    prepopulated_fields = {"slug": ("titulo",)}

@admin.register(Promocional)
class PromocionalAdmin(admin.ModelAdmin):
    list_display = ('titulo','idioma','date_update')
    prepopulated_fields = {"slug": ("titulo",)}

@admin.register(ConsumerGoods)
class ConsumerGoodsAdmin(admin.ModelAdmin):
    list_display = ('titulo', 'idioma', 'date_update')
    prepopulated_fields = {"slug": ("titulo",)}

@admin.register(FoodBeverage)
class FoodBeverageAdmin(admin.ModelAdmin):
    list_display = ('titulo', 'idioma', 'date_update')
    prepopulated_fields = {"slug": ("titulo",)}

