# -*- coding:utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('produto', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='construcao',
            name='idioma',
            field=models.CharField(max_length=15, verbose_name='Idioma', choices=[(b'en', 'English'), (b'pt-br', 'Portugues'), (b'fr', 'French')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='eletronico',
            name='idioma',
            field=models.CharField(max_length=15, verbose_name='Idioma', choices=[(b'en', 'English'), (b'pt-br', 'Portugues'), (b'fr', 'French')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='promocional',
            name='idioma',
            field=models.CharField(max_length=15, verbose_name='Idioma', choices=[(b'en', 'English'), (b'pt-br', 'Portugues'), (b'fr', 'French')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='textil',
            name='idioma',
            field=models.CharField(max_length=15, verbose_name='Idioma', choices=[(b'en', 'English'), (b'pt-br', 'Portugues'), (b'fr', 'French')]),
            preserve_default=True,
        ),
    ]
