# -*- coding:utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Construcao',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('titulo', models.CharField(max_length=150, verbose_name='Titulo')),
                ('texto', ckeditor.fields.RichTextField(null=True, verbose_name='Texto', blank=True)),
                ('slug', models.SlugField(null=True, max_length=150, blank=True, unique=True, verbose_name='URL')),
                ('idioma', models.CharField(max_length=15, verbose_name='Idioma')),
                ('date_joined', models.DateTimeField(auto_now_add=True, verbose_name=b'Criado em')),
                ('date_update', models.DateTimeField(auto_now=True, verbose_name=b'Atualizado em')),
            ],
            options={
                'verbose_name': 'Construcao',
                'verbose_name_plural': 'Construcoes',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Eletronico',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('titulo', models.CharField(max_length=150, verbose_name='Titulo')),
                ('texto', ckeditor.fields.RichTextField(null=True, verbose_name='Texto', blank=True)),
                ('slug', models.SlugField(null=True, max_length=150, blank=True, unique=True, verbose_name='URL')),
                ('idioma', models.CharField(max_length=15, verbose_name='Idioma')),
                ('date_joined', models.DateTimeField(auto_now_add=True, verbose_name=b'Criado em')),
                ('date_update', models.DateTimeField(auto_now=True, verbose_name=b'Atualizado em')),
            ],
            options={
                'verbose_name': 'Eletronico',
                'verbose_name_plural': 'Eletronicos',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Promocional',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('titulo', models.CharField(max_length=150, verbose_name='Titulo')),
                ('texto', ckeditor.fields.RichTextField(null=True, verbose_name='Texto', blank=True)),
                ('slug', models.SlugField(null=True, max_length=150, blank=True, unique=True, verbose_name='URL')),
                ('idioma', models.CharField(max_length=15, verbose_name='Idioma')),
                ('date_joined', models.DateTimeField(auto_now_add=True, verbose_name=b'Criado em')),
                ('date_update', models.DateTimeField(auto_now=True, verbose_name=b'Atualizado em')),
            ],
            options={
                'verbose_name': 'Promocional',
                'verbose_name_plural': 'Promocionais',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Textil',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('titulo', models.CharField(max_length=150, verbose_name='Titulo')),
                ('texto', ckeditor.fields.RichTextField(null=True, verbose_name='Texto', blank=True)),
                ('slug', models.SlugField(null=True, max_length=150, blank=True, unique=True, verbose_name='URL')),
                ('idioma', models.CharField(max_length=15, verbose_name='Idioma')),
                ('date_joined', models.DateTimeField(auto_now_add=True, verbose_name=b'Criado em')),
                ('date_update', models.DateTimeField(auto_now=True, verbose_name=b'Atualizado em')),
            ],
            options={
                'verbose_name': 'Textil',
                'verbose_name_plural': 'Texteis',
            },
            bases=(models.Model,),
        ),
    ]
