# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('produto', '0003_auto_20151002_1434'),
    ]

    operations = [
        migrations.CreateModel(
            name='ConsumerGoods',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('titulo', models.CharField(max_length=150, verbose_name='Titulo')),
                ('texto', ckeditor.fields.RichTextField(null=True, verbose_name='Texto', blank=True)),
                ('slug', models.SlugField(null=True, max_length=150, blank=True, unique=True, verbose_name='URL')),
                ('idioma', models.CharField(max_length=15, verbose_name='Idioma', choices=[(b'en', 'English'), (b'fr', 'French'), (b'pt-br', 'Portugues')])),
                ('date_joined', models.DateTimeField(auto_now_add=True, verbose_name=b'Criado em')),
                ('date_update', models.DateTimeField(auto_now=True, verbose_name=b'Atualizado em')),
            ],
            options={
                'verbose_name': 'Bens e consumo',
                'verbose_name_plural': 'Bens e consumo',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='FoodBeverage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('titulo', models.CharField(max_length=150, verbose_name='Titulo')),
                ('texto', ckeditor.fields.RichTextField(null=True, verbose_name='Texto', blank=True)),
                ('slug', models.SlugField(null=True, max_length=150, blank=True, unique=True, verbose_name='URL')),
                ('idioma', models.CharField(max_length=15, verbose_name='Idioma', choices=[(b'en', 'English'), (b'fr', 'French'), (b'pt-br', 'Portugues')])),
                ('date_joined', models.DateTimeField(auto_now_add=True, verbose_name=b'Criado em')),
                ('date_update', models.DateTimeField(auto_now=True, verbose_name=b'Atualizado em')),
            ],
            options={
                'verbose_name': 'Alimento e bebida',
                'verbose_name_plural': 'Alimentos e bebidas',
            },
            bases=(models.Model,),
        ),
    ]
