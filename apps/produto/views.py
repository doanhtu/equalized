# -*- coding:utf-8 -*-
from django.shortcuts import render_to_response, HttpResponse
from apps.produto.models import *
from django.conf import settings
from django.db.models import Q
import datetime
from django.utils.translation import get_language
from django.views.generic import View, TemplateView, ListView, DetailView
from django.utils.html import strip_tags

class TextilListView(ListView):
    
    template_name = 'produto/textil.html'
    model = Textil
    context_object_name = 'texteis'

    def get_queryset(self):

        idioma = get_language()
        texteis = Textil.objects.filter(idioma=idioma)
        queryset = texteis

        return queryset

class EletronicoListView(ListView):

    template_name = 'produto/eletronicos.html'
    model = Eletronico
    context_object_name = 'eletronicos'

    def get_queryset(self):

        idioma = get_language()
        eletronicos = Eletronico.objects.filter(idioma=idioma)
        queryset = eletronicos

        return queryset

class ConstrucaoListView(ListView):

    template_name = 'produto/construcao.html'
    model = Construcao
    context_object_name = 'construcoes'

    def get_queryset(self):

        idioma = get_language()
        construcoes = Construcao.objects.filter(idioma=idioma)
        queryset = construcoes

        return queryset

class PromocionalListView(ListView):

    template_name = 'produto/produtos-promocionais.html'
    model = Promocional
    context_object_name = 'promocionais'

    def get_queryset(self):

        idioma = get_language()
        promocionais = Promocional.objects.filter(idioma=idioma)
        queryset = promocionais

        return queryset

class ConsumerGoodsListView(ListView):

    template_name = 'produto/bens-e-consumo.html'
    model = ConsumerGoods
    context_object_name = 'consumer_goods'

    def get_queryset(self):

        idioma = get_language()
        consumer_goods = ConsumerGoods.objects.filter(idioma=idioma)
        queryset = consumer_goods

        return queryset

class FoodBeverageListView(ListView):

    template_name = 'produto/alimentos-e-bebidas.html'
    model = FoodBeverage
    context_object_name = 'foods_beverages'

    def get_queryset(self):

        idioma = get_language()
        foods_beverages = FoodBeverage.objects.filter(idioma=idioma)
        queryset = foods_beverages

        return queryset
