# -*- coding:utf-8 -*-
from django import forms
from django.utils.translation import ugettext as _
from django.utils.translation import ugettext_lazy as _

EMAIL_CHOICES = (
        ('',_('Selecione')),
        ('textile@equalizeltd.com', 'textile@equalizeltd.com'),
        ('construction@equalizeltd.com', 'construction@equalizeltd.com'),
        ('electronics@equalizeltd.com', 'electronics@equalizeltd.com'),
        ('gifts@equalizeltd.com', 'gifts@equalizeltd.com'),
        ('info@equalizeltd.com', 'info@equalizeltd.com'),
    )

class ContactForm(forms.Form):

    nome = forms.CharField(label=_('Nome'), max_length=150, widget=forms.TextInput(attrs={'placeholder': _('Nome'),'class':'col-xs-12 required'}))
    sobrenome = forms.CharField(label=_('Sobrenome'), max_length=150, widget=forms.TextInput(attrs={'placeholder': _('Sobrenome'),'class':'col-xs-12 required'}))
    email = forms.EmailField(label=_('Seu e-mail'), max_length=200, widget=forms.TextInput(attrs={'placeholder': _('Seu e-mail'),'class':'col-xs-12 required email esq'}))
    departamento = forms.ChoiceField(label=_('Departamento'), choices=EMAIL_CHOICES, widget=forms.Select(attrs={'class':'col-xs-12 required email esq'}))
    mensagem = forms.CharField(widget=forms.Textarea(attrs={'placeholder': _('Mensagem'),'class':'col-xs-12 required', 'cols':'', 'rows':'10'}),label=_('Mensagem'))
    arquivo = forms.FileField(label=_('Arquivo'), widget=forms.FileInput(attrs={'placeholder' : _('Selecione um arquivo')}))
    fake_file = forms.CharField(widget=forms.TextInput(attrs={'placeholder': _('Selecione um arquivo'),'class':'col-xs-12'}))