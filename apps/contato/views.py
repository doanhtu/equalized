# -*- coding:utf-8 -*-
from django.shortcuts import render_to_response, HttpResponse
from django.template import RequestContext

from django.views.generic import View, FormView
from django.conf import settings
from apps.contato.forms import *
import json
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.core.mail import EmailMultiAlternatives, BadHeaderError
from django.core.urlresolvers import reverse

class ContatoView(FormView):

    # formulário renderizado
    form_class = ContactForm

    content_type = 'form'

    success_url = '/contato/?m=1' 

    # Template que o método get irá renderizar
    template_name = 'contato/index.html'

    # Template do e-mail que será enviado
    template_email = 'contato/email/index.html'

    # Endereço de envio do e-mail
    email_to = ['guimoncks@terra.com.br', ]
    #email_to = ['cristianpiero@gmail.com', ]
    email_from = settings.EMAIL_HOST_USER
    # Assunto padrão para o e-mail
    email_subject = 'Contato Enviado pelo site.'

    def get(self, request, *args, **kwargs):

        form = self.form_class
        email_enviado = 0
        if request.GET:
            email_enviado = request.GET['m']

        return self.render_to_response(self.get_context_data(
            form=form,
            email_enviado=email_enviado))

    def post(self, request, *args, **kwargs):

        form_class = self.get_form_class()
        form = self.get_form(form_class)

        if form.is_valid():
            self.email_to = form.cleaned_data['departamento']
            attach = request.FILES['arquivo']
            self.sender(form, attach)
            return self.form_valid(form)
        else:

            print self.form_invalid
            return self.form_invalid(form)
    

    def sender(self, form, attach):
        '''
        Retorna os dados limpos do formulário e envia para função que faz o
        envio
        '''
        data = {
            'item':form,
            }
        
        html_content = render_to_string(self.template_email, data)
        return self.send_email(self.email_subject, form.cleaned_data['email'], html_content, [self.email_to], attach)
        
    def send_email(self, subject, email_from, html_content, email_to, attach):

        text_content = html_content

        try:
            mail = EmailMultiAlternatives(subject, text_content, email_from, email_to)
            mail.attach_alternative(html_content, 'text/html')
            mail.attach(attach.name, attach.read(), attach.content_type)
            mail.send()
            return True
        except BadHeaderError:
            return False    


