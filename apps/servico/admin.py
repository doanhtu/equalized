# -*- coding:utf-8 -*-
from django.contrib import admin
from apps.servico.models import *
from apps.servico.forms import *

@admin.register(Consultoria)
class ConsultoriaAdmin(admin.ModelAdmin):
    list_display = ('titulo','idioma','date_update')
    prepopulated_fields = {"slug": ("titulo",)}

@admin.register(Pesquisa)
class PesquisaAdmin(admin.ModelAdmin):
    list_display = ('titulo','idioma','date_update')
    prepopulated_fields = {"slug": ("titulo",)}

@admin.register(Producao)
class ProducaoAdmin(admin.ModelAdmin):
    list_display = ('titulo','idioma','date_update')
    prepopulated_fields = {"slug": ("titulo",)}

@admin.register(Logistica)
class LogisticaAdmin(admin.ModelAdmin):
    list_display = ('titulo','idioma','date_update')
    prepopulated_fields = {"slug": ("titulo",)}

