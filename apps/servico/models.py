# -*- coding:utf-8 -*-
from apps import settings
from django.db import models
import datetime
import os
from uuid import uuid4
from django.utils.deconstruct import deconstructible
from ckeditor.fields import RichTextField
from django.contrib.auth.models import User
from sorl.thumbnail import ImageField
from django.template.defaultfilters import slugify

class Consultoria(models.Model):

    class Meta:
        verbose_name = 'Consultoria'
        verbose_name_plural = 'Consultorias'

    titulo = models.CharField(u'Titulo', max_length=150)
    texto = RichTextField(u'Texto',blank=True,null=True)
    slug = models.SlugField(u'URL', max_length=150,blank=True,null=True,unique=True)
    idioma = models.CharField(u'Idioma', choices=settings.LANGUAGES, max_length=15)
    date_joined = models.DateTimeField('Criado em',auto_now_add=True)
    date_update = models.DateTimeField('Atualizado em',auto_now=True)

    def __unicode__(self):
        return self.titulo

class Pesquisa(models.Model):

    class Meta:
        verbose_name = 'Pesquisa'
        verbose_name_plural = 'Pesquisas'

    titulo = models.CharField(u'Titulo', max_length=150)
    texto = RichTextField(u'Texto',blank=True,null=True)
    slug = models.SlugField(u'URL', max_length=150,blank=True,null=True,unique=True)
    idioma = models.CharField(u'Idioma', choices=settings.LANGUAGES, max_length=15)
    date_joined = models.DateTimeField('Criado em',auto_now_add=True)
    date_update = models.DateTimeField('Atualizado em',auto_now=True)

    def __unicode__(self):
        return self.titulo

class Producao(models.Model):

    class Meta:
        verbose_name = 'Producao'
        verbose_name_plural = 'Producoes'

    titulo = models.CharField(u'Titulo', max_length=150)
    texto = RichTextField(u'Texto',blank=True,null=True)
    slug = models.SlugField(u'URL', max_length=150,blank=True,null=True,unique=True)
    idioma = models.CharField(u'Idioma', choices=settings.LANGUAGES, max_length=15)
    date_joined = models.DateTimeField('Criado em',auto_now_add=True)
    date_update = models.DateTimeField('Atualizado em',auto_now=True)

    def __unicode__(self):
        return self.titulo

class Logistica(models.Model):

    class Meta:
        verbose_name = 'Logistica'
        verbose_name_plural = 'Logisticas'

    titulo = models.CharField(u'Titulo', max_length=150)
    texto = RichTextField(u'Texto',blank=True,null=True)
    slug = models.SlugField(u'URL', max_length=150,blank=True,null=True,unique=True)
    idioma = models.CharField(u'Idioma', choices=settings.LANGUAGES, max_length=15)
    date_joined = models.DateTimeField('Criado em',auto_now_add=True)
    date_update = models.DateTimeField('Atualizado em',auto_now=True)

    def __unicode__(self):
        return self.titulo
