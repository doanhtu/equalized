#-*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
from apps.servico.views import *
from django.utils.translation import ugettext_lazy as _

urlpatterns = patterns('apps.servico.views',

    url(_(r'^consultoria/$'), ConsultoriaListView.as_view(), name="consultoria"),
    url(_(r'^pesquisa-mercado/$'), PesquisaListView.as_view(), name="pesquisa"),
    url(_(r'^producao/$'), ProducaoListView.as_view(), name="producao"),
    url(_(r'^logistica/$'), LogisticaListView.as_view(), name="logistica"),
)
