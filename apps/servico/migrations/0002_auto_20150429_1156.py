# -*- coding:utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('servico', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='pesquisa',
            options={'verbose_name': 'Pesquisa', 'verbose_name_plural': 'Pesquisas'},
        ),
        migrations.AlterField(
            model_name='consultoria',
            name='idioma',
            field=models.CharField(max_length=15, verbose_name='Idioma', choices=[(b'en', 'English'), (b'pt-br', 'Portugues'), (b'fr', 'French')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='logistica',
            name='idioma',
            field=models.CharField(max_length=15, verbose_name='Idioma', choices=[(b'en', 'English'), (b'pt-br', 'Portugues'), (b'fr', 'French')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='pesquisa',
            name='idioma',
            field=models.CharField(max_length=15, verbose_name='Idioma', choices=[(b'en', 'English'), (b'pt-br', 'Portugues'), (b'fr', 'French')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='producao',
            name='idioma',
            field=models.CharField(max_length=15, verbose_name='Idioma', choices=[(b'en', 'English'), (b'pt-br', 'Portugues'), (b'fr', 'French')]),
            preserve_default=True,
        ),
    ]
