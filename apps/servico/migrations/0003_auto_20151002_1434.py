# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('servico', '0002_auto_20150429_1156'),
    ]

    operations = [
        migrations.AlterField(
            model_name='consultoria',
            name='idioma',
            field=models.CharField(max_length=15, verbose_name='Idioma', choices=[(b'en', 'English'), (b'fr', 'French'), (b'pt-br', 'Portugues')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='logistica',
            name='idioma',
            field=models.CharField(max_length=15, verbose_name='Idioma', choices=[(b'en', 'English'), (b'fr', 'French'), (b'pt-br', 'Portugues')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='pesquisa',
            name='idioma',
            field=models.CharField(max_length=15, verbose_name='Idioma', choices=[(b'en', 'English'), (b'fr', 'French'), (b'pt-br', 'Portugues')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='producao',
            name='idioma',
            field=models.CharField(max_length=15, verbose_name='Idioma', choices=[(b'en', 'English'), (b'fr', 'French'), (b'pt-br', 'Portugues')]),
            preserve_default=True,
        ),
    ]
