# -*- coding:utf-8 -*-
from django import template
from apps.noticia.models import *
from django.db.models import Q
from django.db.models import Count
register = template.Library()
import datetime

@register.inclusion_tag('noticia/templatetags/categorias.html')
def categorias():
    now = datetime.datetime.now() 

    q = Post.objects.filter(Q(ativo=True) & Q(data_publicacao__lte=now)).values('categorias__id','categorias__nome','categorias__slug').annotate(dcount=Count('categorias__id')).order_by('categorias__nome')
    
    return {'items': q,}



@register.inclusion_tag('noticia/templatetags/posts_recentes.html')
def posts_recentes():
    now = datetime.datetime.now() 
    q = Post.objects.filter(Q(ativo=True) & Q(data_publicacao__lte=now)).order_by('-data_publicacao')[:5]
    return {'items': q,}


@register.inclusion_tag('noticia/templatetags/tags.html')
def tags():
    now = datetime.datetime.now()     
    
    q = Post.objects.filter(Q(ativo=True) & Q(data_publicacao__lte=now)).values('tags__id','tags__nome','tags__slug').annotate(dcount=Count('tags__id')).order_by('tags__nome')
    
    return {'items': q,}


@register.inclusion_tag('noticia/templatetags/arquivo.html')
def arquivo():
    now = datetime.datetime.now() 
    q = Post.objects.filter(Q(ativo=True) & Q(data_publicacao__lte=now)).extra(select={'year': "EXTRACT(year FROM data_publicacao)"}).values('year').annotate(dcount=Count('data_publicacao')).order_by('-data_publicacao')
    
    return {'items': q,}


@register.inclusion_tag('nav_ingresso.html')
def ingresso_menu():    
    return {'items': '',}

@register.inclusion_tag('nav_ensino.html')
def ensino_menu():    
    return {'items': '',}


@register.inclusion_tag('nav_nucleos.html')
def nucleos_menu():    
    return {'items': '',}

@register.inclusion_tag('nav_institucional.html')
def institucional_menu():    
    return {'items': '',}    

@register.inclusion_tag('nav_two.html')
def two_menu():    
    return {'items': '',}    

@register.inclusion_tag('nav_two.html')
def two_li_menu():    
    return {'li': True,}  

@register.inclusion_tag('nav_top.html')
def top_menu():    
    return {'items': '',}    

@register.inclusion_tag('nav_top.html')
def top_li_menu():    
    return {'li': True,}  