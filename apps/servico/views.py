# -*- coding:utf-8 -*-
from django.shortcuts import render_to_response, HttpResponse
from apps.servico.models import *
from django.conf import settings
from django.db.models import Q
import datetime
from django.utils.translation import get_language
from django.views.generic import View, TemplateView, ListView, DetailView
from django.utils.html import strip_tags

class ConsultoriaListView(ListView):
    
    template_name = 'servico/consultoria.html'

    model = Consultoria

    context_object_name = 'consultorias'

    def get_queryset(self):
        idioma = get_language()
        consultorias = Consultoria.objects.all().filter(idioma=idioma)
        queryset = consultorias
        return queryset

class PesquisaListView(ListView):

    template_name = 'servico/pesquisa-de-mercado.html'

    model = Pesquisa

    context_object_name = 'pesquisas'

    def get_queryset(self):
        idioma = get_language()
        pesquisas = Pesquisa.objects.filter(idioma=idioma)
        queryset = pesquisas
        return queryset

class ProducaoListView(ListView):

    template_name = 'servico/producao.html'

    model = Producao

    context_object_name = 'producoes'

    def get_queryset(self):
        idioma = get_language()
        producoes = Producao.objects.filter(idioma=idioma)
        queryset = producoes
        return queryset

class LogisticaListView(ListView):

    template_name = 'servico/logistica.html'

    model = Logistica

    context_object_name = 'logisticas'

    def get_queryset(self):
        idioma = get_language()
        logisticas = Logistica.objects.filter(idioma=idioma)
        queryset = logisticas
        return queryset
