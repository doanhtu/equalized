# -*- coding:utf-8 -*-
from apps import settings
from django.db import models
import datetime
import os
from uuid import uuid4
from django.utils.deconstruct import deconstructible
from ckeditor.fields import RichTextField
from django.contrib.auth.models import User
from sorl.thumbnail import ImageField
from django.template.defaultfilters import slugify

@deconstructible
class PathAndRename(object):

    def __init__(self, sub_path):
        self.path = sub_path

    def __call__(self, instance, filename):
        ext = filename.split('.')
        # set filename as random string
        filename = '%s.%s'%(slugify(ext[0]), ext[-1])
        # return the whole path to the file
        return os.path.join(self.path, filename)

path = "uploads/{:%Y/%m}/".format(datetime.datetime.now())
path_and_rename = PathAndRename(path)

class Introducao(models.Model):

    class Meta:
        verbose_name = 'Introducao'
        verbose_name_plural = 'Introducoes'

    titulo = models.CharField(u'Titulo', max_length=150)
    texto = RichTextField(u'Texto',blank=True,null=True)
    slug = models.SlugField(u'URL', max_length=150,blank=True,null=True,unique=True)
    idioma = models.CharField(u'Idioma', choices=settings.LANGUAGES, max_length=15)
    date_joined = models.DateTimeField('Criado em',auto_now_add=True)
    date_update = models.DateTimeField('Atualizado em',auto_now=True)

    def __unicode__(self):
        return self.titulo

class Implantacao(models.Model):

    class Meta:
        verbose_name = 'Implantacao'
        verbose_name_plural = 'Implantacoes'

    titulo = models.CharField(u'Titulo', max_length=150)
    texto = RichTextField(u'Texto',blank=True,null=True)
    slug = models.SlugField(u'URL', max_length=150,blank=True,null=True,unique=True)
    idioma = models.CharField(u'Idioma', choices=settings.LANGUAGES, max_length=15)
    date_joined = models.DateTimeField('Criado em',auto_now_add=True)
    date_update = models.DateTimeField('Atualizado em',auto_now=True)

    def __unicode__(self):
        return self.titulo

class Numero(models.Model):

    class Meta:
        verbose_name = 'Numero'
        verbose_name_plural = 'Numeros'

    titulo = models.CharField(u'Titulo', max_length=150)
    texto = RichTextField(u'Texto',blank=True,null=True)
    slug = models.SlugField(u'URL', max_length=150,blank=True,null=True,unique=True)
    idioma = models.CharField(u'Idioma', choices=settings.LANGUAGES, max_length=15)
    date_joined = models.DateTimeField('Criado em',auto_now_add=True)
    date_update = models.DateTimeField('Atualizado em',auto_now=True)

    def __unicode__(self):
        return self.titulo

class Pergunta(models.Model):

    class Meta:
        verbose_name = 'Pergunta'
        verbose_name_plural = 'Perguntas'

    titulo = models.CharField(u'Titulo', max_length=150)
    texto = RichTextField(u'Texto',blank=True,null=True)
    slug = models.SlugField(u'URL', max_length=150,blank=True,null=True,unique=True)
    idioma = models.CharField(u'Idioma', choices=settings.LANGUAGES, max_length=15)
    imagem = models.ImageField(u'Imagem', upload_to=path_and_rename, max_length=255,null=True,blank=True,default=None)
    date_joined = models.DateTimeField('Criado em',auto_now_add=True)
    date_update = models.DateTimeField('Atualizado em',auto_now=True)

    def __unicode__(self):
        return self.titulo
