# -*- coding:utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Implementacao',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('titulo', models.CharField(max_length=150, verbose_name='Titulo')),
                ('texto', ckeditor.fields.RichTextField(null=True, verbose_name='Texto', blank=True)),
                ('slug', models.SlugField(null=True, max_length=150, blank=True, unique=True, verbose_name='URL')),
                ('idioma', models.CharField(max_length=15, verbose_name='Idioma')),
                ('date_joined', models.DateTimeField(auto_now_add=True, verbose_name=b'Criado em')),
                ('date_update', models.DateTimeField(auto_now=True, verbose_name=b'Atualizado em')),
            ],
            options={
                'verbose_name': 'Implementacao',
                'verbose_name_plural': 'Implementacoes',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Introducao',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('titulo', models.CharField(max_length=150, verbose_name='Titulo')),
                ('texto', ckeditor.fields.RichTextField(null=True, verbose_name='Texto', blank=True)),
                ('slug', models.SlugField(null=True, max_length=150, blank=True, unique=True, verbose_name='URL')),
                ('idioma', models.CharField(max_length=15, verbose_name='Idioma')),
                ('date_joined', models.DateTimeField(auto_now_add=True, verbose_name=b'Criado em')),
                ('date_update', models.DateTimeField(auto_now=True, verbose_name=b'Atualizado em')),
            ],
            options={
                'verbose_name': 'Introducao',
                'verbose_name_plural': 'Introducoes',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Numero',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('titulo', models.CharField(max_length=150, verbose_name='Titulo')),
                ('texto', ckeditor.fields.RichTextField(null=True, verbose_name='Texto', blank=True)),
                ('slug', models.SlugField(null=True, max_length=150, blank=True, unique=True, verbose_name='URL')),
                ('idioma', models.CharField(max_length=15, verbose_name='Idioma')),
                ('date_joined', models.DateTimeField(auto_now_add=True, verbose_name=b'Criado em')),
                ('date_update', models.DateTimeField(auto_now=True, verbose_name=b'Atualizado em')),
            ],
            options={
                'verbose_name': 'Numero',
                'verbose_name_plural': 'Numeros',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Pergunta',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('titulo', models.CharField(max_length=150, verbose_name='Titulo')),
                ('texto', ckeditor.fields.RichTextField(null=True, verbose_name='Texto', blank=True)),
                ('slug', models.SlugField(null=True, max_length=150, blank=True, unique=True, verbose_name='URL')),
                ('idioma', models.CharField(max_length=15, verbose_name='Idioma')),
                ('date_joined', models.DateTimeField(auto_now_add=True, verbose_name=b'Criado em')),
                ('date_update', models.DateTimeField(auto_now=True, verbose_name=b'Atualizado em')),
            ],
            options={
                'verbose_name': 'Pergunta',
                'verbose_name_plural': 'Perguntas',
            },
            bases=(models.Model,),
        ),
    ]
