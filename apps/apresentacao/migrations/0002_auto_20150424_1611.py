# -*- coding:utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('apresentacao', '0001_initial'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Implementacao',
            new_name='Implantacao',
        ),
        migrations.AlterModelOptions(
            name='implantacao',
            options={'verbose_name': 'Implantacao', 'verbose_name_plural': 'Implantacoes'},
        ),
    ]
