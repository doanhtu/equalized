# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('apresentacao', '0004_auto_20151001_1130'),
    ]

    operations = [
        migrations.AlterField(
            model_name='implantacao',
            name='idioma',
            field=models.CharField(max_length=15, verbose_name='Idioma', choices=[(b'en', 'English'), (b'fr', 'French'), (b'pt-br', 'Portugues')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='introducao',
            name='idioma',
            field=models.CharField(max_length=15, verbose_name='Idioma', choices=[(b'en', 'English'), (b'fr', 'French'), (b'pt-br', 'Portugues')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='numero',
            name='idioma',
            field=models.CharField(max_length=15, verbose_name='Idioma', choices=[(b'en', 'English'), (b'fr', 'French'), (b'pt-br', 'Portugues')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='pergunta',
            name='idioma',
            field=models.CharField(max_length=15, verbose_name='Idioma', choices=[(b'en', 'English'), (b'fr', 'French'), (b'pt-br', 'Portugues')]),
            preserve_default=True,
        ),
    ]
