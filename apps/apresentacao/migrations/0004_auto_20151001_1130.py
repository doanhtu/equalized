# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import apps.apresentacao.models


class Migration(migrations.Migration):

    dependencies = [
        ('apresentacao', '0003_auto_20150429_1156'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pergunta',
            name='imagem',
            field=models.ImageField(default=None, upload_to=apps.apresentacao.models.PathAndRename(b'uploads/2015/10/'), max_length=255, blank=True, null=True, verbose_name='Imagem'),
            preserve_default=True,
        ),
    ]
