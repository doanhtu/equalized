# -*- coding:utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import apps.apresentacao.models


class Migration(migrations.Migration):

    dependencies = [
        ('apresentacao', '0002_auto_20150424_1611'),
    ]

    operations = [
        migrations.AddField(
            model_name='pergunta',
            name='imagem',
            field=models.ImageField(default=None, upload_to=apps.apresentacao.models.PathAndRename(b'uploads/2015/04/'), max_length=255, blank=True, null=True, verbose_name='Imagem'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='implantacao',
            name='idioma',
            field=models.CharField(max_length=15, verbose_name='Idioma', choices=[(b'en', 'English'), (b'pt-br', 'Portugues'), (b'fr', 'French')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='introducao',
            name='idioma',
            field=models.CharField(max_length=15, verbose_name='Idioma', choices=[(b'en', 'English'), (b'pt-br', 'Portugues'), (b'fr', 'French')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='numero',
            name='idioma',
            field=models.CharField(max_length=15, verbose_name='Idioma', choices=[(b'en', 'English'), (b'pt-br', 'Portugues'), (b'fr', 'French')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='pergunta',
            name='idioma',
            field=models.CharField(max_length=15, verbose_name='Idioma', choices=[(b'en', 'English'), (b'pt-br', 'Portugues'), (b'fr', 'French')]),
            preserve_default=True,
        ),
    ]
