# -*- coding:utf-8 -*-
from django.conf.urls import patterns, include, url
from apps.apresentacao.views import *
from django.utils.translation import ugettext_lazy as _

urlpatterns = patterns('apps.apresentacao.views',

    url(_(r'^introducao/$'), IntroducaoListView.as_view(), name="introducao"),
    url(_(r'^implantacoes/$'), ImplantacaoListView.as_view(), name="implantacoes"),
    url(_(r'^numeros/$'), NumeroListView.as_view(), name="numeros"),
    url(_(r'^suas-perguntas/$'), PerguntaListView.as_view(), name="suas-perguntas"),

)
