# -*- coding:utf-8 -*-
from django.contrib import admin
from apps.apresentacao.models import *
from apps.apresentacao.forms import *

@admin.register(Introducao)
class IntroducaoAdmin(admin.ModelAdmin):
    list_display = ('titulo','idioma','date_update')
    prepopulated_fields = {"slug": ("titulo",)}

@admin.register(Implantacao)
class ImplementacaoAdmin(admin.ModelAdmin):
    list_display = ('titulo','idioma','date_update')
    prepopulated_fields = {"slug": ("titulo",)}

@admin.register(Numero)
class NumeroAdmin(admin.ModelAdmin):
    list_display = ('titulo','idioma','date_update')
    prepopulated_fields = {"slug": ("titulo",)}

@admin.register(Pergunta)
class PerguntaAdmin(admin.ModelAdmin):
    list_display = ('titulo','idioma','date_update')
    prepopulated_fields = {"slug": ("titulo",)}

