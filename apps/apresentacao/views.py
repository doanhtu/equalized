# -*- coding:utf-8 -*-
from itertools import chain
from django.core.serializers import json
from django.shortcuts import render_to_response, HttpResponse
from apps.apresentacao.models import *
from apps.noticia.models import *
from django.conf import settings
from django.db.models import Q
import datetime
from django.utils import translation
from django.utils.translation import get_language, check_for_language
from django.views.generic import View, TemplateView, ListView, DetailView
from django.utils.html import strip_tags

import json, requests
from pprint import pprint

class HomeView(ListView):

    template_name = 'base.html'
    model = Post
    context_object_name = 'posts'

    now = datetime.datetime.now()

    def get_queryset(self):
        idioma = get_language()
        nf_list = Post.objects.filter(Q(ativo=True) & Q(data_publicacao__lte=self.now) & Q(fixar=True) & Q(idioma=idioma)).order_by('-data_publicacao')[:4]

        n_list = []
        count_n = int(nf_list.count())

        if count_n < 4:
            limit = 4-count_n
            n_list = Post.objects.filter(Q(ativo=True) & Q(data_publicacao__lte=self.now) & Q(fixar=False) & Q(idioma=idioma)).order_by('-data_publicacao')[:limit]


        nitems = list(chain(nf_list, n_list))

        nitems = sorted(nitems, key=lambda instance: instance.data_publicacao,reverse=True)
        nitems = sorted(nitems, key=lambda instance: instance.fixar,reverse=True)

        queryset = nitems
        return queryset

    def render_to_response(self, context, **response_kwargs):
        moedas = {}

        r = requests.get('http://www.apilayer.net/api/live?access_key=c86d3db52bd4dbbd6e6a496dea03f0e8')
        r = json.loads(r._content)

        moedas['USD'] = None
        moedas['EUR'] = None
        moedas['BRL'] = None
        moedas['GBP'] = None
        moedas['INR'] = None
        moedas['CNY'] = None

        if r['quotes']['USDUSD']:
            moedas['USD'] = r['quotes']['USDUSD']
        if r['quotes']['USDEUR']:
            moedas['EUR'] = r['quotes']['USDEUR']
        if r['quotes']['USDBRL']:
            moedas['BRL'] = r['quotes']['USDBRL']
        if r['quotes']['USDGBP']:
            moedas['GBP'] = r['quotes']['USDGBP']
        if r['quotes']['USDINR']:
            moedas['INR'] = r['quotes']['USDINR']
        if r['quotes']['USDCNY']:
            moedas['CNY'] = r['quotes']['USDCNY']


        context['moedas'] = moedas

        response_kwargs.setdefault('content_type', self.content_type)
        return self.response_class(
            request=self.request,
            template=self.get_template_names(),
            context=context,
            **response_kwargs
        )

class LanguageView(ListView):

    template_name = 'select-language.html'

    def post(self, request, *args, **kwargs):

        from django import http

        user_language = request.POST['language']
        translation.activate(user_language)
        response = http.HttpResponseRedirect('home')
        response.set_cookie(settings.LANGUAGE_COOKIE_NAME, user_language)

        return response

    def get(self, request, *args, **kwargs):

        from django import http
        user_language = kwargs['language']
        translation.activate(user_language)
        response = http.HttpResponseRedirect('/home')
        response.set_cookie(settings.LANGUAGE_COOKIE_NAME, user_language)
        return response

class IntroducaoListView(ListView):

    template_name = 'apresentacao/introducao.html'

    model = Introducao

    context_object_name = 'introducoes'

    def get_queryset(self):
        idioma = get_language()
        introducoes = Introducao.objects.filter(idioma=idioma)
        queryset = introducoes
        return queryset

class ImplantacaoListView(ListView):

    template_name = 'apresentacao/implantacoes.html'

    model = Implantacao

    context_object_name = 'implantacoes'

    def get_queryset(self):
        idioma = get_language()

        print idioma

        implantacoes = Implantacao.objects.filter(idioma=idioma)
        queryset = implantacoes
        return queryset

class NumeroListView(ListView):

    template_name = 'apresentacao/numeros.html'

    model = Numero

    context_object_name = 'numeros'

    def get_queryset(self):
        idioma = get_language()
        numeros = Numero.objects.filter(idioma=idioma)
        queryset = numeros
        return queryset

class PerguntaListView(ListView):

    template_name = 'apresentacao/suas-perguntas.html'

    model = Pergunta

    context_object_name = 'perguntas'

    def get_queryset(self):
        idioma = get_language()
        perguntas = Pergunta.objects.filter(idioma=idioma)
        queryset = perguntas
        return queryset
