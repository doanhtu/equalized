# -*- coding:utf-8 -*-
from django.conf.urls import patterns, include, url
from django.templatetags.i18n import language
from django.views.generic import TemplateView
from django.conf import settings
from django.contrib import admin
from apps.apresentacao.views import *
from apps.contato.views import *
from apps.cotacao.views import *

from django.conf.urls.i18n import i18n_patterns
from django.core.urlresolvers import reverse
from django.utils.translation import activate

from django.utils.translation import ugettext_lazy as _


# urlpatterns = patterns('',
#     # Examples:
#     url(r'^$', TemplateView.as_view(template_name="select-language.html"), name="language"),
# )

urlpatterns = [
    # url(r'^$', LanguageView.as_view()),
    url(r'^$', TemplateView.as_view(template_name="select-language.html"), name="language"),
    url(r'^set_language/(?P<language>[\w-]+)$', LanguageView.as_view(), name="set_language"),
]

urlpatterns += i18n_patterns(
    url(r'^i18n/', include('django.conf.urls.i18n')),
    url(_(r'^home/$'), HomeView.as_view(), name="home"),
    url(_(r'^apresentacao/'), include('apps.apresentacao.urls', namespace="apresentacao")),
    url(_(r'^nossos-servicos/'), include('apps.servico.urls', namespace="servico")),
    url(_(r'^produtos/'), include('apps.produto.urls', namespace="produto")),
    url(_(r'^noticias/'), include('apps.noticia.urls', namespace="noticia")),
    url(_(r'^obter-cotacao/'), CotacaoView.as_view(), name="cotacao"),
    url(_(r'^contato/'), ContatoView.as_view(), name="contato"),

    # #url de terceiros
    url(r'^grappelli/', include('grappelli.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^ckeditor/', include('ckeditor.urls')),
)

if settings.DEBUG:
    # static files (images, css, javascript, etc.)
    urlpatterns += patterns('',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
        url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
    )