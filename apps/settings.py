# -*- coding:utf-8 -*-
from unipath import Path
PROJECT_DIR = Path(__file__).parent

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

import datetime

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '$9v*!_-d&jvwwkmbk8mey9dcprr2^v3-sk)3v)wsqz46n-0oh6'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

TEMPLATE_DEBUG = False

ALLOWED_HOSTS = ['www.equalizeltd.com','equalizeltd.com','equalizeltd.testesbeagleship.com.br']

# Application definition

INSTALLED_APPS = (
    #Apps de terceiros
    'django_wysiwyg',
    'ckeditor',
    'sorl.thumbnail',
    'grappelli',
    #'compressor',

    #django
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    #Minhas apps
    'apps.noticia',
    'apps.apresentacao',
    'apps.servico',
    'apps.produto',
)

#AUTH_USER_MODEL = 'apps.account.Profile'

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'apps.urls'

WSGI_APPLICATION = 'apps.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

# Use a Sqlite database by default
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'dj-equalizeltd',
        'USER': 'root',
        'PASSWORD': 'VLmatkhau123h!@#',
        'HOST': 'localhost',
        'PORT': '3306',
        }
}

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'pt-BR'

TIME_ZONE = 'America/Sao_Paulo'

USE_I18N = True

USE_L10N = True

USE_TZ = True

# LANGUAGES = (
#     ('en', 'English'),
#     ('pt-br', 'Brazilian'),
#     ('fr', 'French'),
# )

from django.utils.translation import ugettext_lazy as _
LANGUAGES = (
    ('en', _('English')),
    ('fr', _('French')),
    ('pt-br', _('Portugues')),
)

LOCALE_PATHS = (PROJECT_DIR.child('locale'),)


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/
STATIC_ROOT =  ''#os.path.join(PROJECT_DIR.parent, 'static')
STATIC_URL = '/static/'

MEDIA_ROOT = os.path.join(PROJECT_DIR.parent, 'media')
MEDIA_URL = '/media/'

STATICFILES_DIRS = (
    os.path.join(PROJECT_DIR.parent, 'static'),
)


TEMPLATE_DIRS = (
    PROJECT_DIR.child('templates'),

)
DJANGO_WYSIWYG_FLAVOR = "ckeditor"

path = "uploads/{:%Y/%m}/".format(datetime.datetime.now())

CKEDITOR_UPLOAD_PATH = path

CKEDITOR_JQUERY_URL = '//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js'

CKEDITOR_CONFIGS = {
           'default': {
               'toolbar': [
                ["Format", "TextColor", "Bold", "Italic", "Underline", "Strike", "SpellChecker"],
                ['Cut','Copy','Paste','PasteText','PasteFromWord',"RemoveFormat",],
                ['NumberedList', 'BulletedList', "Indent", "Outdent", 'JustifyLeft', 'JustifyCenter',
                 'JustifyRight', 'JustifyBlock',"Blockquote"],
                ["Image", "Table", "Link", "Unlink", "Anchor", "SectionLink", "Subscript", "Superscript"], ['Undo', 'Redo'], ["Source"],
                ["Maximize"]],

           },
       }


TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.static",
    "django.core.context_processors.tz",
    "django.contrib.messages.context_processors.messages",
    'django.core.context_processors.request',
)

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    # other finders..

)

GRAPPELLI_ADMIN_TITLE = 'Equalize Ltd'

# The host to use for sending email.
EMAIL_HOST = 'smtp.googlemail.com'

# Username to use for the SMTP server defined in EMAIL_HOST.
DEFAULT_FROM_EMAIL = EMAIL_HOST_USER = 'noreply@mr2digital.com.br'

# Password to use for the SMTP server defined in EMAIL_HOST.
EMAIL_HOST_PASSWORD = 'nor@mr2!digital'

# Port to use for the SMTP server defined in EMAIL_HOST.
EMAIL_PORT = '587'

# Whether to use a TLS (secure) connection when talking to the SMTP server.
EMAIL_USE_TLS = True

DECIMAL_SEPARATOR = ','
USE_THOUSAND_SEPARATOR = False
DATE_FORMAT = '%d/%m/%Y'
DATE_INPUT_FORMATS = ('%d/%m/%Y',)

# Try and import local settings which can be used to override any of the above.
try:
    from settings_local import *
except ImportError:
    pass
