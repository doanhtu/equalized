# -*- coding:utf-8 -*-
from django.contrib import admin
from apps.noticia.models import *
from apps.noticia.forms import *
from sorl.thumbnail.admin import AdminImageMixin

@admin.register(CategoriaPost)
class CategoriaAdmin(admin.ModelAdmin):
    list_display = ('nome','categoria_pai','ativo','date_update')
    prepopulated_fields = {"slug": ("nome",)}


@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    list_display = ('nome','ativo','date_update')
    prepopulated_fields = {"slug": ("nome",)}

class ArquivoAdmin(admin.TabularInline):
    model = Arquivo
    extra = 1

@admin.register(Post)
class PostAdmin(AdminImageMixin, admin.ModelAdmin):
    form = PostForm
    list_display = ('data_publicacao','titulo','fixar','ativo','ordem','date_update','idioma')
    search_fields = ('texto', 'titulo')
    list_filter = ['ativo','data_publicacao','categorias']    
    prepopulated_fields = {"slug": ("titulo",)}
    readonly_fields = ('date_update', 'ultima_modificacao',)
    filter_horizontal = ('categorias','tags')
    #exclude = ('tags',)

    
    """inlines = [
        ArquivoAdmin,
    ] """
    def save_model(self, request, obj, form, change):
        if not obj.autor.id:
            obj.autor = request.user
        obj.ultima_modificacao = request.user
        obj.save()