# -*- coding:utf-8 -*-
from django.forms import ModelForm
from django.contrib.auth.models import User
from apps.noticia.models import Post

class PostForm(ModelForm):
    # class Meta:
    #     model = Post

    def clean_autor(self):
        if not self.cleaned_data['autor']:
            return User()
        return self.cleaned_data['autor']

    def clean_ultima_modificacao(self):
        if not self.cleaned_data['ultima_modificacao']:
            return User()
        return self.cleaned_data['ultima_modificacao']