# -*- coding:utf-8 -*-
from apps import settings
from django.db import models
import datetime
import os
from uuid import uuid4
from django.utils.deconstruct import deconstructible
from ckeditor.fields import RichTextField
from django.contrib.auth.models import User
from sorl.thumbnail import ImageField
from django.template.defaultfilters import slugify

BOLEAN_CHOISES = (   
      (0,'Não'),
      (1,'Sim'),      
    )

STATUS_CHOISES = (   
      (0,'Rascunho'),
      (1,'Publicar'),      
    )

@deconstructible
class PathAndRename(object):

    def __init__(self, sub_path):
        self.path = sub_path

    def __call__(self, instance, filename):
        ext = filename.split('.')        
        # set filename as random string
        filename = '%s.%s'%(slugify(ext[0]), ext[-1])
        # return the whole path to the file
        return os.path.join(self.path, filename)

path = "uploads/{:%Y/%m}/".format(datetime.datetime.now())        
path_and_rename = PathAndRename(path)


class CategoriaPost(models.Model): 

    class Meta:
        verbose_name = 'Categoria'
        verbose_name_plural = 'Categorias'    

    nome = models.CharField(u'Nome', max_length=150)
    slug = models.SlugField(u'URL', max_length=150,blank=True,null=True,unique=True)    
    ativo = models.BooleanField(u'Ativo',default=1)
    categoria_pai = models.ForeignKey(u'self', null=True,blank=True, related_name='self__categoria')
    imagem = models.ImageField(u'Imagem', upload_to=path_and_rename, max_length=255,null=True,blank=True,default=None)
    idioma = models.CharField(u'Idioma', choices=settings.LANGUAGES, max_length=15, null=True, blank=True)
    date_joined = models.DateTimeField('Criado em',auto_now_add=True)
    date_update = models.DateTimeField('Atualizado em',auto_now=True)

    def __unicode__(self):
        return self.nome


class Tag(models.Model): 

    class Meta:
        verbose_name = 'Tag'
        verbose_name_plural = 'Tags'
    
    nome = models.CharField(u'Tag', max_length=150)    
    slug = models.SlugField(u'URL', max_length=150,blank=True,null=True,unique=True)
    ativo = models.BooleanField(u'Ativo',default=1)
    idioma = models.CharField(u'Idioma', choices=settings.LANGUAGES, max_length=15, null=True, blank=True)
    date_joined = models.DateTimeField('Criado em',auto_now_add=True)
    date_update = models.DateTimeField('Atualizado em',auto_now=True)

    def __unicode__(self):
        return self.nome        

class Post(models.Model): 

    class Meta:
        verbose_name = 'Post'
        verbose_name_plural = 'Posts'
        ordering = ['-data_publicacao']
    
    data_publicacao = models.DateTimeField('Data de Publicação',null=False,blank=False)
    titulo = models.CharField(u'Titulo', max_length=150)    
    texto = RichTextField(u'Texto',blank=True,null=True)
    slug = models.SlugField(u'URL', max_length=150,blank=True,null=True,unique=True)
    imagem = models.ImageField(u'Imagem',help_text="720x300", upload_to=path_and_rename, max_length=255,null=True,blank=True,default=None)
    fixar = models.BooleanField(u'Fixar na Home',default=False) 
    ativo = models.BooleanField(u'Ativo',default=1)
    ordem = models.PositiveSmallIntegerField(u'Ordem',default=1)
    categorias = models.ManyToManyField(CategoriaPost, null=True, blank=True)
    tags = models.ManyToManyField(Tag, null=True, blank=True)
    autor = models.ForeignKey(User,verbose_name="Autor", default=None, related_name='autor_post', blank=True, null=True)
    date_joined = models.DateTimeField('Criado em',auto_now_add=True)
    ultima_modificacao = models.ForeignKey(User,verbose_name="modificado por", default=None, related_name='modificacao_post', blank=True, null=True)
    date_update = models.DateTimeField('Modificado em',auto_now=True)
    idioma = models.CharField(u'Idioma', choices=settings.LANGUAGES, max_length=15, null=True, blank=True)
       
    
    def __unicode__(self):
        return self.titulo

class Arquivo(models.Model): 

    class Meta:
        verbose_name = 'Arquivo'
        verbose_name_plural = 'Arquivos'
    
    post = models.ForeignKey(Post,verbose_name="Post", default=None, blank=True, null=True)   
    titulo = models.CharField(u'Titulo do Arquivo', max_length=150,null=True,blank=True)    
    arquivo = models.FileField(u'Arquivo', upload_to=path_and_rename, max_length=255,null=True,blank=True,default=None)
    ativo = models.BooleanField(u'Ativo',default=1)
    date_joined = models.DateTimeField(auto_now_add=True)
    date_update = models.DateTimeField(auto_now=True)
    idioma = models.CharField(u'Idioma', choices=settings.LANGUAGES, max_length=15, null=True, blank=True)

    def __unicode__(self):
        return self.titulo
