# -*- coding:utf-8 -*-
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.shortcuts import render_to_response, HttpResponse

from apps.noticia.models import *

from django.conf import settings
from django.db.models import Q
import datetime
from django.utils.translation import get_language
from django.views.generic import View, TemplateView, ListView, DetailView
from django.utils.html import strip_tags


class PostListView(ListView):

    template_name = 'noticia/index.html'
    paginate_by = 4
    context_object_name = 'items'
    queryset = None
    now = datetime.datetime.now()

    def get_queryset(self):

        idioma = get_language()
        items = Post.objects.filter(Q(ativo=True) & Q(data_publicacao__lte=self.now) & Q(idioma=idioma)).order_by('-data_publicacao')

        for i in items:
            if not i.imagem:
                for x in i.categorias.all():
                    if x.imagem:
                        i.imagem_categoria = x.imagem
                        break

        queryset = items
        return queryset

    def get_context_data(self, *args, **kwargs):
        context = super(PostListView, self).get_context_data(*args, **kwargs)
        idioma = get_language()
        context['last_item'] = Post.objects.filter(Q(ativo=True) & Q(data_publicacao__lte=self.now) & Q(idioma=idioma)).order_by('-data_publicacao').last()
        return context

    def get_template_names(self):

        template_name = self.template_name

        if self.request.is_ajax():
            template_name = 'noticia/index_ajax.html'

        return [template_name]

class TagPostView(PostListView):

    def get(self,request,*args,**kwargs):

        items = Post.objects.filter(Q(ativo=True) & Q(data_publicacao__lte=self.now) & Q(tags__slug=kwargs.get('slug'))).order_by('-data_publicacao')

        for i in items:
            if not i.imagem:
                for x in i.categorias.all():
                    if x.imagem:
                        i.imagem_categoria = x.imagem
                        break

        self.queryset = items

        return super(TagPostView, self).get(request, *args, **kwargs)

class CategoriaPostView(PostListView):

    def get(self,request,*args,**kwargs):

        items = Post.objects.filter(Q(ativo=True) & Q(data_publicacao__lte=self.now) & Q(categorias__slug=kwargs.get('slug'))).order_by('-data_publicacao')

        for i in items:
            if not i.imagem:
                for x in i.categorias.all():
                    if x.imagem:
                        i.imagem_categoria = x.imagem
                        break

        self.queryset = items

        return super(CategoriaPostView, self).get(request, *args, **kwargs)


class ArquivoPostView(PostListView):

    def get(self,request,*args,**kwargs):

        items = Post.objects.filter(Q(ativo=True) & Q(data_publicacao__year=kwargs.get('ano')) & Q(data_publicacao__lte=self.now)).order_by('-data_publicacao')[:4]

        for i in items:
            if not i.imagem:
                for x in i.categorias.all():
                    if x.imagem:
                        i.imagem_categoria = x.imagem
                        break

        self.queryset = items

        return super(ArquivoPostView, self).get(request, *args, **kwargs)

class PostView(DetailView):

    template_name = 'noticia/post.html'

    model = Post

    context_object_name = 'item'

    def get_object(self,queryset=None):

        """
        Returns the object the view is displaying.
        By default this requires `self.queryset` and a `pk` or `slug` argument
        in the URLconf, but subclasses can override this to return any object.
        """
        # Use a custom queryset if provided; this is required for subclasses
        # like DateDetailView
        if queryset is None:
            queryset = self.get_queryset()
        # Next, try looking up by primary key.
        pk = self.kwargs.get(self.pk_url_kwarg, None)
        slug = self.kwargs.get(self.slug_url_kwarg, None)
        if pk is not None:
            queryset = queryset.filter(pk=pk)
        # Next, try looking up by slug.
        elif slug is not None:
            slug_field = self.get_slug_field()
            queryset = queryset.filter(**{slug_field: slug})
        # If none of those are defined, it's an error.
        else:
            raise AttributeError("Generic detail view %s must be called with "
                                 "either an object pk or a slug."
                                 % self.__class__.__name__)
        try:
            # Get the single item from the filtered queryset
            obj = queryset.get()
            if not obj.imagem:
                for x in obj.categorias.all():

                    if x.imagem:
                        obj.imagem_categoria = x.imagem
                        break

        except queryset.model.DoesNotExist:
            raise Http404(_("No %(verbose_name)s found matching the query") %
                          {'verbose_name': queryset.model._meta.verbose_name})
        return obj




class DownloadView(View):

    def get(self, request, *args, **kwargs):

        import mimetypes
        from os import path

        arquivo = "%s/%s" % (settings.MEDIA_ROOT, kwargs.get('pdf'))

        if not (path.exists(arquivo)):
            raise Http404()

        mimetype, encoding = mimetypes.guess_type(arquivo)

        if mimetype is None:
            mimetype = 'application/force-download'

        file = arquivo.split("/")[-1]

        response = HttpResponse(open(arquivo, 'r').read())
        response['Content-Type'] = mimetype
        response['Pragma'] = 'public'
        response['Expires'] = '0'
        response['Cache-Control'] = 'must-revalidate, post-check=0, pre-check=0'
        response['Content-Disposition'] = 'attachment; filename=%s' % file
        response['Content-Transfer-Encoding'] = 'binary'
        response['Content-Length'] = str(path.getsize(arquivo))
        return response

