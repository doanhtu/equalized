#-*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url

from apps.noticia.views import *
from django.utils.translation import ugettext_lazy as _

urlpatterns = patterns('apps.noticia.views',
    url(r'^$', PostListView.as_view(), name="posts"),
    url(_(r'^tag/(?P<slug>.+?)/$'), TagPostView.as_view(), name="tag"),
    url(_(r'^categoria/(?P<slug>.+?)/$'), CategoriaPostView.as_view(), name="categoria"),
    url(_(r'^arquivo/(?P<ano>\d+)/$'), ArquivoPostView.as_view(), name="arquivo"),
    url(_(r'^download/(?P<pdf>.+?)$'), DownloadView.as_view(), name="download"),
    url(_(r'^(?P<slug>.+?)/$'), PostView.as_view(), name="post"),
)
