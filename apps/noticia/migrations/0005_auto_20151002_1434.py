# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('noticia', '0004_auto_20151001_1130'),
    ]

    operations = [
        migrations.AlterField(
            model_name='arquivo',
            name='idioma',
            field=models.CharField(blank=True, max_length=15, null=True, verbose_name='Idioma', choices=[(b'en', 'English'), (b'fr', 'French'), (b'pt-br', 'Portugues')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='categoriapost',
            name='idioma',
            field=models.CharField(blank=True, max_length=15, null=True, verbose_name='Idioma', choices=[(b'en', 'English'), (b'fr', 'French'), (b'pt-br', 'Portugues')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='post',
            name='idioma',
            field=models.CharField(blank=True, max_length=15, null=True, verbose_name='Idioma', choices=[(b'en', 'English'), (b'fr', 'French'), (b'pt-br', 'Portugues')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='tag',
            name='idioma',
            field=models.CharField(blank=True, max_length=15, null=True, verbose_name='Idioma', choices=[(b'en', 'English'), (b'fr', 'French'), (b'pt-br', 'Portugues')]),
            preserve_default=True,
        ),
    ]
