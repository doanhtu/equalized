# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import apps.noticia.models


class Migration(migrations.Migration):

    dependencies = [
        ('noticia', '0003_auto_20150429_1616'),
    ]

    operations = [
        migrations.AlterField(
            model_name='arquivo',
            name='arquivo',
            field=models.FileField(default=None, upload_to=apps.noticia.models.PathAndRename(b'uploads/2015/10/'), max_length=255, blank=True, null=True, verbose_name='Arquivo'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='categoriapost',
            name='imagem',
            field=models.ImageField(default=None, upload_to=apps.noticia.models.PathAndRename(b'uploads/2015/10/'), max_length=255, blank=True, null=True, verbose_name='Imagem'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='post',
            name='imagem',
            field=models.ImageField(default=None, upload_to=apps.noticia.models.PathAndRename(b'uploads/2015/10/'), max_length=255, blank=True, help_text=b'720x300', null=True, verbose_name='Imagem'),
            preserve_default=True,
        ),
    ]
