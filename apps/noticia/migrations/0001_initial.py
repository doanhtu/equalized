# -*- coding:utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import ckeditor.fields
import apps.noticia.models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Arquivo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('titulo', models.CharField(max_length=150, null=True, verbose_name='Titulo do Arquivo', blank=True)),
                ('arquivo', models.FileField(default=None, upload_to=apps.noticia.models.PathAndRename(b'uploads/2015-2-12/'), max_length=255, blank=True, null=True, verbose_name='Arquivo')),
                ('ativo', models.BooleanField(default=1, verbose_name='Ativo')),
                ('date_joined', models.DateTimeField(auto_now_add=True)),
                ('date_update', models.DateTimeField(auto_now=True)),
            ],
            options={
                'verbose_name': 'Arquivo',
                'verbose_name_plural': 'Arquivos',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='CategoriaPost',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nome', models.CharField(max_length=150, verbose_name='Nome')),
                ('slug', models.SlugField(null=True, max_length=150, blank=True, unique=True, verbose_name='URL')),
                ('ativo', models.BooleanField(default=1, verbose_name='Ativo')),
                ('imagem', models.ImageField(default=None, upload_to=apps.noticia.models.PathAndRename(b'uploads/2015-2-12/'), max_length=255, blank=True, null=True, verbose_name='Imagem')),
                ('date_joined', models.DateTimeField(auto_now_add=True, verbose_name=b'Criado em')),
                ('date_update', models.DateTimeField(auto_now=True, verbose_name=b'Atualizado em')),
                ('categoria_pai', models.ForeignKey(related_name='self__categoria', blank=True, to='noticia.CategoriaPost', null=True)),
            ],
            options={
                'verbose_name': 'Categoria',
                'verbose_name_plural': 'Categorias',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Post',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('data_publicacao', models.DateTimeField(verbose_name=b'Data de Publica\xc3\xa7\xc3\xa3o')),
                ('titulo', models.CharField(max_length=150, verbose_name='Titulo')),
                ('texto', ckeditor.fields.RichTextField(null=True, verbose_name='Texto', blank=True)),
                ('slug', models.SlugField(null=True, max_length=150, blank=True, unique=True, verbose_name='URL')),
                ('imagem', models.ImageField(default=None, upload_to=apps.noticia.models.PathAndRename(b'uploads/2015-2-12/'), max_length=255, blank=True, help_text=b'720x300', null=True, verbose_name='Imagem')),
                ('fixar', models.BooleanField(default=False, verbose_name='Fixar na Home')),
                ('ativo', models.BooleanField(default=1, verbose_name='Ativo')),
                ('ordem', models.PositiveSmallIntegerField(default=1, verbose_name='Ordem')),
                ('date_joined', models.DateTimeField(auto_now_add=True, verbose_name=b'Criado em')),
                ('date_update', models.DateTimeField(auto_now=True, verbose_name=b'Modificado em')),
                ('autor', models.ForeignKey(related_name='autor_post', default=None, blank=True, to=settings.AUTH_USER_MODEL, null=True, verbose_name=b'Autor')),
                ('categorias', models.ManyToManyField(to='noticia.CategoriaPost', null=True, blank=True)),
            ],
            options={
                'ordering': ['-data_publicacao'],
                'verbose_name': 'Post',
                'verbose_name_plural': 'Posts',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Tag',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nome', models.CharField(max_length=150, verbose_name='Tag')),
                ('slug', models.SlugField(null=True, max_length=150, blank=True, unique=True, verbose_name='URL')),
                ('ativo', models.BooleanField(default=1, verbose_name='Ativo')),
                ('date_joined', models.DateTimeField(auto_now_add=True, verbose_name=b'Criado em')),
                ('date_update', models.DateTimeField(auto_now=True, verbose_name=b'Atualizado em')),
            ],
            options={
                'verbose_name': 'Tag',
                'verbose_name_plural': 'Tags',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='post',
            name='tags',
            field=models.ManyToManyField(to='noticia.Tag', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='post',
            name='ultima_modificacao',
            field=models.ForeignKey(related_name='modificacao_post', default=None, blank=True, to=settings.AUTH_USER_MODEL, null=True, verbose_name=b'modificado por'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='arquivo',
            name='post',
            field=models.ForeignKey(default=None, blank=True, to='noticia.Post', null=True, verbose_name=b'Post'),
            preserve_default=True,
        ),
    ]
