# -*- coding:utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('noticia', '0002_auto_20150422_1609'),
    ]

    operations = [
        migrations.AddField(
            model_name='arquivo',
            name='idioma',
            field=models.CharField(blank=True, max_length=15, null=True, verbose_name='Idioma', choices=[(b'en', 'English'), (b'pt-br', 'Portugues'), (b'fr', 'French')]),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='categoriapost',
            name='idioma',
            field=models.CharField(blank=True, max_length=15, null=True, verbose_name='Idioma', choices=[(b'en', 'English'), (b'pt-br', 'Portugues'), (b'fr', 'French')]),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='post',
            name='idioma',
            field=models.CharField(blank=True, max_length=15, null=True, verbose_name='Idioma', choices=[(b'en', 'English'), (b'pt-br', 'Portugues'), (b'fr', 'French')]),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='tag',
            name='idioma',
            field=models.CharField(blank=True, max_length=15, null=True, verbose_name='Idioma', choices=[(b'en', 'English'), (b'pt-br', 'Portugues'), (b'fr', 'French')]),
            preserve_default=True,
        ),
    ]
