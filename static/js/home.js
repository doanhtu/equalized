var	slideInterval = 4200;
var slideShowActive = 0;
var slideShowHome = function slideShowHome() {
	var $slides = $('.slideshow-bg').find('.slideshow-slide');
	var slidesLength = $slides.length;	
	var slideTransition = 700;
	

	$('.slideshow-slide').eq(slideShowActive).velocity({opacity: 0}, {duration: slideTransition});
	slideShowActive++;
	if(slideShowActive  === slidesLength) {
		slideShowActive = 0;
	}
	$('.slideshow-slide').eq(slideShowActive).velocity({top: [0, -100]}, {duration: slideInterval + 2000, ease: 'linear'});
	$('.slideshow-slide').eq(slideShowActive).velocity({opacity: 1}, {duration: slideTransition, queue: false});

}

$(function() {

	$('.carosel-header .swiper-container').swiper({
		pagination: '.swiper-pagination',
		paginationClickable: true,
		grabCursor: true
	});

	$('.slideshow-slide').eq(slideShowActive).velocity({top: [0, -100]}, {duration: slideInterval + 2000, ease: 'linear'});
	setInterval(function(){
		slideShowHome();
	}, slideInterval);
});



