$(function() {


    $('a.vermais').unbind("click").click(function(){
    	var $this = $(this)
    	var page = parseInt($this.attr('data-page'))+1;
    	var url = $this.attr('rel');
    	var total_page = parseInt($this.attr('data-numpages'));
    	$this.children('span').text('Carregando...');
    	$.ajax({
			type: 'get',
			url: url,
			data: {'page':page},
			error: function(retorno){ alert('Falhou, tente novamente!'); },
			success: function(data)
			{
				$('.list-all-news').append(data);
				$this.attr('data-page',page);
				$this.children('span').text('Ver mais');
			}
		});
		if (page >= total_page){
			$this.hide();
		}
		return false;
    })
});

