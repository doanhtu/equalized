$(function() {
    $('#mobile-open-menu').on("click", function() {
        var $nav = $("#nav");
        if (!$("#nav").hasClass("active")) {
            $(this).addClass("active");
            $nav.addClass("active");            
            return false;
        }
        else {
            $(this).removeClass("active");
            $nav.removeClass("active");
        }
    });

    $(".nav-li-a").on('click', function(e) {
        if ($(this).next().hasClass('nav-submenu')) {
            $('.nav-li').removeClass('active');
            $(this).parent().toggleClass('active');
            $(this).closest(".nav-submenu").css('height', '220px');
            e.preventDefault();
        }
    });
    $('.custom-upload input[type=file]').change(function(){
        $(this).next().find('input').val($(this).val());
    });
});